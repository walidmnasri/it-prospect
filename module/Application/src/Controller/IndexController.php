<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\Contacts;
use Application\Model\ContactsRepository;
use Application\Model\Newsletters;
use Application\Model\NewslettersRepository;
use Application\Model\Quote;
use Application\Model\QuoteRepository;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{

    /**
     * @var ContactsRepository
     */
    private $table;
    /**
     * @var NewslettersRepository
     */
    private $newsletterTable;

    /**
     * @var QuoteRepository
     */
    private $quoteTable;

    /**
     * IndexController constructor.
     * @param ContactsRepository $contactTable
     * @param NewslettersRepository $newsletterTable
     * @param QuoteRepository $quoteTable
     */
    public function __construct(ContactsRepository $contactTable, NewslettersRepository $newsletterTable, QuoteRepository $quoteTable)
    {
        $this->table = $contactTable;
        $this->newsletterTable = $newsletterTable;
        $this->quoteTable = $quoteTable;
    }

    public function indexAction()
    {
        $data['page'] = 'home';
        return $data;
    }

    public function servicesAction()
    {
        $data['page'] = 'services';
        return $data;
    }

    public function studiesAction()
    {
        $data['page'] = 'studies';
        return $data;
    }

    public function sectorsAction()
    {
        $data['page'] = 'sectors';
        return $data;
    }

    public function statflowAction()
    {
        $data['page'] = 'statflow';
        return $data;
    }

    public function contactsAction()
    {
        $data['page'] = 'contacts';
        return $data;
    }

    public function contactUsAction()
    {
        $contact = new Contacts();
        $contact->setFullName(utf8_decode($this->params()->fromPost('name')));
        $contact->setEmail($this->params()->fromPost('email'));
        $contact->setTelephone($this->params()->fromPost('phone'));
        $contact->setWebsite($this->params()->fromPost('permalink'));
        $contact->setMessage(utf8_decode($this->params()->fromPost('message')));

        $this->table->saveContact($contact);
        exit;
    }

    public function quoteAction()
    {
        $sector = $this->params()->fromPost('statflow-sector')
            ? $this->params()->fromPost('statflow-sector')
            : $this->params()->fromPost('survey-sector');
        $sendType = $this->params()->fromPost('statflow-sendType')
            ? $this->params()->fromPost('statflow-sendType')
            : $this->params()->fromPost('survey-sendType');
        $sendFlow = $this->params()->fromPost('statflow-sendFlow')
            ? $this->params()->fromPost('statflow-sendFlow')
            : $this->params()->fromPost('survey-sendFlow');

        $quote = new Quote();
        $quote->setFullName(utf8_decode($this->params()->fromPost('name')));
        $quote->setEmail($this->params()->fromPost('email'));
        $quote->setTelephone($this->params()->fromPost('phone'));
        $quote->setService($this->params()->fromPost('service'));
        $quote->setDescription(utf8_decode($this->params()->fromPost('description')));
        $quote->setSector((int) $sector);
        $quote->setSendFlow((int) $sendFlow);
        $quote->setSendType((int) $sendType);
        $quote->setAgenceNumber((int) $this->params()->fromPost('agenceNumber'));
        $quote->setSendFrequency((int) $this->params()->fromPost('sendFrequency'));
        $quote->setCrm((int) $this->params()->fromPost('crm'));
        $quote->setPeriod((int) $this->params()->fromPost('period'));

        $this->quoteTable->save($quote);
        exit;
    }

    public function newsletterAction()
    {
        $newsletter = new Newsletters();
        $newsletter->setEmail($this->params()->fromPost('email'));

        $this->newsletterTable->save($newsletter);
        exit;
    }
}
