<?php
/**
 * Created by PhpStorm.
 * User: wmnasri
 * Date: 03/10/17
 * Time: 01:53
 */

namespace Application\Model;

use Zend\Db\TableGateway\TableGatewayInterface;


class QuoteRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function save(Quote $quote)
    {
        $data = [
            'fullName' => $quote->getFullName(),
            'email' => $quote->getEmail(),
            'adressIp' => $quote->getAdressIp(),
            'device' => $quote->getDevice(),
            'telephone' => $quote->getTelephone(),
            'service' => $quote->getService(),
            'description' => $quote->getDescription(),
            'sector' => $quote->getSector(),
            'crm' => $quote->getCrm(),
            'agenceNumber' => $quote->getAgenceNumber(),
            'sendFlow' => $quote->getSendFlow(),
            'sendFrequency' => $quote->getSendFrequency(),
            'sendType' => $quote->getSendType(),
            'period' => $quote->getPeriod(),
        ];

        $this->tableGateway->insert($data);
        $config = [
            'charset' => 'UTF-8',
            'from' => [
                'webmaster@it-prospect.com' => 'WebMaster'
            ],
            'to' => [
                'support@it-prospect.com' => 'Support IT Prospect',
                'mnasri.walid@it-prospect.com' => 'Walid MNASRI'
            ],
            'subject' => 'Nouveau Devis sur IT PROSPECT',
            'fields' => $data,
        ];


        return Mail::send($config);

    }

}