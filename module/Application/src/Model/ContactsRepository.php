<?php
/**
 * Created by PhpStorm.
 * User: wmnasri
 * Date: 03/10/17
 * Time: 01:53
 */

namespace Application\Model;

use Zend\Db\TableGateway\TableGatewayInterface;

class ContactsRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function saveContact(Contacts $contacts)
    {
        $data = [
            'fullName' => $contacts->getFullName(),
            'email' => $contacts->getEmail(),
            'adressIp' => $contacts->getAdressIp(),
            'device' => $contacts->getDevice(),
            'telephone' => $contacts->getTelephone(),
            'website' => $contacts->getWebsite(),
            'message' => $contacts->getMessage(),
        ];

        $this->tableGateway->insert($data);
        $config = [
            'charset' => 'UTF-8',
            'from' => [
                'webmaster@it-prospect.com' => 'WebMaster'
            ],
            'to' => [
                'contact@it-prospect.com' => 'Contact IT Prospect',
                'mnasri.walid@it-prospect.com' => 'Walid MNASRI'
            ],
            'subject' => 'Nouveau Contact sur IT PROSPECT',
            'fields' => [
                'Name' => $contacts->getFullName(),
                'Email' => $contacts->getEmail(),
                'Phone' => $contacts->getTelephone(),
                'Website' => $contacts->getWebsite(),
                'Message' => $contacts->getMessage()
            ],
        ];


        return Mail::send($config);
    }

}
