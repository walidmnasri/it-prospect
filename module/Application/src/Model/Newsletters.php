<?php
/**
 * Created by PhpStorm.
 * User: wmnasri
 * Date: 03/10/17
 * Time: 01:49
 */
namespace Application\Model;

class Newsletters
{
    protected $id;
    protected $email;
    protected $adressIp;
    protected $device;

public function exchangeArray(){}
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAdressIp()
    {
        return $this->adressIp;
    }

    /**
     * @param mixed $adressIp
     */
    public function setAdressIp($adressIp)
    {
        $this->adressIp = $adressIp;
    }

    /**
     * @return mixed
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param mixed $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
    }
}
