<?php
/**
 * Created by PhpStorm.
 * User: wmnasri
 * Date: 03/10/17
 * Time: 01:49
 */
namespace Application\Model;

class Quote
{
    protected $id;
    protected $fullName;
    protected $email;
    protected $telephone;
    protected $service;
    protected $description;
    protected $adressIp;
    protected $device;
    protected $sector;
    protected $agenceNumber;
    protected $sendFlow;
    protected $sendType;
    protected $sendFrequency;
    protected $crm;
    protected $period;

    public function exchangeArray(){}

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getAdressIp()
    {
        return $this->adressIp;
    }

    /**
     * @param mixed $adressIp
     */
    public function setAdressIp($adressIp)
    {
        $this->adressIp = $adressIp;
    }

    /**
     * @return mixed
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param mixed $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
    }

    /**
     * @return mixed
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * @param mixed $sector
     */
    public function setSector($sector)
    {
        $this->sector = $sector;
    }

    /**
     * @return mixed
     */
    public function getAgenceNumber()
    {
        return $this->agenceNumber;
    }

    /**
     * @param mixed $agenceNumber
     */
    public function setAgenceNumber($agenceNumber)
    {
        $this->agenceNumber = $agenceNumber;
    }

    /**
     * @return mixed
     */
    public function getSendFlow()
    {
        return $this->sendFlow;
    }

    /**
     * @param mixed $sendFlow
     */
    public function setSendFlow($sendFlow)
    {
        $this->sendFlow = $sendFlow;
    }

    /**
     * @return mixed
     */
    public function getSendType()
    {
        return $this->sendType;
    }

    /**
     * @param mixed $sendType
     */
    public function setSendType($sendType)
    {
        $this->sendType = $sendType;
    }

    /**
     * @return mixed
     */
    public function getSendFrequency()
    {
        return $this->sendFrequency;
    }

    /**
     * @param mixed $sendFrequency
     */
    public function setSendFrequency($sendFrequency)
    {
        $this->sendFrequency = $sendFrequency;
    }

    /**
     * @return mixed
     */
    public function getCrm()
    {
        return $this->crm;
    }

    /**
     * @param mixed $crm
     */
    public function setCrm($crm)
    {
        $this->crm = $crm;
    }

    /**
     * @return mixed
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param mixed $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }

}
