<?php
/**
 * Created by PhpStorm.
 * User: wmnasri
 * Date: 03/10/17
 * Time: 01:53
 */

namespace Application\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;


class NewslettersRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function save(Newsletters $newsletter)
    {
        $data = [
            'email' => $newsletter->getEmail(),
            'adressIp' => $newsletter->getAdressIp(),
            'device' => $newsletter->getDevice(),
        ];

        $this->tableGateway->insert($data);
        return;

    }

}