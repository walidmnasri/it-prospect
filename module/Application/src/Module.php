<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\ContactsRepository::class => function($container) {
                    $tableGateway = $container->get(Model\ContactsRepositoryGateway::class);
                    return new Model\ContactsRepository($tableGateway);
                },
                Model\ContactsRepositoryGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Contacts());
                    return new TableGateway('contacts', $dbAdapter, null, $resultSetPrototype);
                },
                Model\NewslettersRepository::class => function($container) {
                    $tableGateway = $container->get(Model\NewslettersRepositoryGateway::class);
                    return new Model\NewslettersRepository($tableGateway);
                },
                Model\NewslettersRepositoryGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Newsletters());
                    return new TableGateway('newslettres', $dbAdapter, null, $resultSetPrototype);
                },
                Model\QuoteRepository::class => function($container) {
                    $tableGateway = $container->get(Model\QuoteRepositoryGateway::class);
                    return new Model\QuoteRepository($tableGateway);
                },
                Model\QuoteRepositoryGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Quote());
                    return new TableGateway('quote', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\IndexController::class => function($container) {
                    return new Controller\IndexController(
                        $container->get(Model\ContactsRepository::class),
                        $container->get(Model\NewslettersRepository::class),
                        $container->get(Model\QuoteRepository::class)
                    );
                },
            ],
        ];
    }
}
